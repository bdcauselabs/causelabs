## CauseLabs assessment project notes

Some coding involved, but more of a DevOps project.

### Monday 10/7

Morning, Vervoe not accessible. Later, the CauseLabs PDF could not be found. Michael sent the PDF via email.

I have a Linux box, MxLinux based on Ubuntu. And a Win7 box. Initially, I tried the Linux box. Installed Apache2, PHP, Laravel. Configured Apache. Laravle was unhappy because it wanted a newer PHP, but apt-get would not install a new PHP because it thought 7.0.33 is the latest. 

Pivoted to the Win7 box because I had previously installed XAMPP there. Now for Laravel, which I had used for two projects in 2014-2015. I looked into Laradock a little but ended up just installing Laravel into the XAMPP tree. This involved creating a virtual host conf file for Apache, host "laravel.tld" and setting 127.0.0.1 to that in the hosts file. Installed Postman (API test utility).

Wrote PHP API routes code.

Attempted to get phpMyAdmin running for MySQL. It had connection problems. I had been playing with the config for a remote connetion, so apparently broke it. 

### Tuesday 10/8

Reinstalled XAMPP. phpMyAdmin was now working fine. Created a DB and table. Configured the vhost again.

Got all three API endpoints working with Postman. Had to disable CSRF protection in Laravel for Postman. For POSTing people, $_POST was empty. Resorted to using file_get_contents("php://input"). That is a new one to me. 

### Wednesday 10/9

Had initially use the GET method for the delete API function. Changed to DELETE. The data came in with a spurious "1" at the end. Web search yielded nothing on this. Resorted to using $pklist = Route::current()->parameters()["pklist"]; to get the data.

Created an account on bitbucket.org for sharing the source, and pushed it up.

Installed Laravel on swsuds.net, a host I am already using, at http://swsuds.net/laravel_causelabs/. Copied up CauseLabs project files. It gets 404 errors on the API. No server logs. Asked tech support. Home page works, however. 

Added a unit test for the delete and get API functions. Attempted one for the post using different approaches, but was not sucessful -- 500 Internal server error . 

### Thursday 10/10

Experimenting with Vue.js -- this is my first use of it ever. Added a folder called temp_client/ to the Laravel tree (pushed to Bitbucket). Coded the client side. Need to get the Laravel on the public site working in order to test (or test with a local server). The client-side functionality needs to be merged into Laravel.


## README

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).