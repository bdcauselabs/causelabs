<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */

    // Removing CRSF protection to allow POST from Postman.
    // For a real site, a solution to this is needed.
    protected $except = [
        '*'
    ];
}
