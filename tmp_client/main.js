
const app = new Vue( {
    el: '#app',

    data: {
        // TODO: get people from API call. That data also has "timestamp" and (full) "name" columns from the DB.
        people: [
            {"pk": 1, "first_name":"Abe", "last_name":"Dee", "age":42, "email":"abe@x.com", "secret":"sauce"},
            {"pk": 2, "first_name":"Cy", "last_name":"Pyter", "age":19, "email": "cy@x.com", "secret":"trick"}, 
            {"pk": 3, "first_name":"Scooter", "last_name":"Mann", "age":34, "email":"scoot@x.com", "secret":"clue"},
            {"pk": 10, "first_name":"Ralin", "last_name":"Kril", "age":73, "email":"ralinkril@x.com", "secret":"saying"},
            {"pk": 11, "first_name":"Kristopher", "last_name":"Hansen", "age":21, "email": "khansen@x.com", "secret":"color"}, 
            {"pk": 31, "first_name":"Mr", "last_name":"T", "age":67, "email":"T@x.com", "secret":"Mind your own business, fool!"}
        ]
    },
    methods: {
        get_people() {
            this.$http.get('/people').then((response) => {   // TODO: test
                this.people = response.people;               // TODO: match the map's key to the PHP and test
            } );
        },

        handle_submit() {
            // Can add one person at a time from the UI, even though the API endpoint can handle a list.
            // TODO: add validation w/ user feedback
            //
            // The requirement says "The secret should allow specification in plain-text, but submit in an encoded fashion."
            // Clarification should be obtained here -- a form submit POST over TLS should suffice. Or, the "submit in encoded 
            // fashion" part could be construed to mean that the secret should be encoded prior to the submit (a fool's errand).
            // HTTPS should always be used, and the home page fetched with the https protocol. 
            let fname  = $('#fname').val();
            let lname  = $('#lname').val();
            let age    = $('#age').val();
            let email  = $('#email').val();
            let secret = $('#secret').val();
            let data   = [{"first_name":fname, "last_name":lname, "age":age, "email":email, "secret":secret}];

            // Using jQuery's post() allows for calling a function after the submit.
            $.post('/people', data, function(data) {      // TODO: test
                // TODO: Inform user of success
                // TODO: Clear the form, probably. Get requirement. 
                this.get_people();   // DB as source of truth; update Vue.
            });
        },

        handle_delete() {
            let pks = $('.selection_checkbox:checked').map(function() {   // TODO: test
                            return $(this).data('pk');
                        }).get();
        
            $.ajax({url: '/people' + pks, type: 'DELETE', success: function(data) {
                            this.get_people();   // DB as source of truth; update Vue.
                        } 
            });
        }
    },
    beforeMount(){
        this.get_people()
    }
});
