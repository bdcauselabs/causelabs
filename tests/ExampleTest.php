<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

// Add "guzzlehttp/guzzle": "^6.3" to requre-dev in composer.json
//use GuzzleHttp\Exception\GuzzleException;
// use GuzzleHttp\Client;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('Laravel 5');
    }

    public function testDelPeople() 
    {
        // Testing that pks 1 and 2 are not deleted by a DELETE request.
        // If a different existing pk is passed, this test will fail (has been manually
        // tested) because the returned data will be the number of records deleted.
        $response = $this->call('DELETE', '/people/1,2');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("0", $response->content());
    }

    public function testGetPeople()
    {
        $response = $this->call('GET', '/people');
        $this->assertEquals(200, $response->getStatusCode());
        $content = $response->content();
        $content_json = json_decode($content, true);   // true: to assoc array
        $this->assertEquals(true, (count($content_json) >= 2));
    }

    // Some different approaches, none of which worked. Laravel documentation itself is very shallow, so
    // listed other resoruces below.
    //
    // Have tested the POST successfully via Postman, however.
    /*
    public function testPostWithJSONBody()
    {
        $rawContent = '{"data": [
            {"first_name":"Abe", "last_name":"Dee", "age":42, "email":"abe@x.com", "secret":"asdf"},
            {"first_name":"Cy", "last_name":"Pyter", "age":19, "email": "cy@x.com", "secret":"qasfwer"}, 
            {"first_name":"Scooter", "last_name":"Mann", "age":34, "email":"scoot@x.com", "secret":"unk"}
            ]
        }';

        // See https://codeday.me/en/qa/20190311/14935.html
        // $response = $this->call('POST', '/people', [], [], [], 
        //     $headers = [
        //         'HTTP_CONTENT_LENGTH' => mb_strlen($rawContent, '8bit'),      
        //         'CONTENT_TYPE' => 'application/json',
        //         'HTTP_ACCEPT' => 'application/json'
        //     ],
        //     json_decode($rawContent, true)
        // );
        // $this->assertEquals(200, $response->getStatusCode());
        
        // See https://www.toptal.com/laravel/restful-laravel-api-tutorial
        //$this->json('POST', '/people', json_decode($rawContent, true) )->seeStatusCode(200);

        // $client = new Client(['laravel.tld:80'], array(
        //     'request.options' => array(
        //         'exceptions' => false,
        //     )
        // ));
        // $request = $client->post('/people', null, $rawContent);

        // See https://medium.com/laravel-5-the-right-way/using-guzzlehttp-with-laravel-1dbea1f633da
        // See https://stackoverflow.com/questions/22244738/how-can-i-use-guzzle-to-send-a-post-request-in-json
        //$client = new Client();
        //$request = $client->post('localhost:80/people', [GuzzleHttp\RequestOptions::JSON => json_decode($rawContent, true) ] );
        //$response = $request->send();
        //$this->assertEquals(200, $response->getStatusCode());
    }
    */
}

