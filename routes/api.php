<?php
/*
See https://www.codementor.io/magarrent/how-to-install-laravel-5-xampp-windows-du107u9ji for
the setting the domain name for XAMPP in C:\xampp\apache\conf\extra\httpd-vhosts.conf (Windows).
Also set `127.0.0.1   laravel.tld` in the C:\Windows\System32\drivers\etc\hosts file.

Postman (ReST utility) POST URLs for XAMPP Laravel.

POST laravel.tld/people
POST Body:
    {"data": [
        {"first_name":"Abe", "last_name":"Dee", "age":42, "email":"abe@x.com", "secret":"asdf"},
        {"first_name":"Cy", "last_name":"Pyter", "age":19, "email": "cy@x.com", "secret":"qasfwer"}, 
        {"first_name":"Scooter", "last_name":"Mann", "age":34, "email":"scoot@x.com", "secret":"unk"}
        ]
    }

GET laravel.tld/people

DELETE laravel.tld/people/6,2
*/

Route::match(['post'], '/people', function () {
    // Create/Add

    // Insert received people data into DB records.
    // In a more elaborate app, could use a model and/or controller. 

    // Disabled CSRF protection via app/Http/Middleware/VerifyCsrfToken.php to use POST w/ Postman.
    // See https://gist.github.com/ethanstenis/3cc78c1d097680ac7ef0
    // See https://laravel.com/docs/5.1/routing#csrf-x-csrf-token
    //echo "\nPOST: " . print_r($_POST, true) . "\n";   // DEBUG; interesting, empty when using POST in Postman...
    //echo "\ninput: ". print_r(json_decode(file_get_contents("php://input"), true)) . "\n";   // DEBUG

    // The `people` is a list of maps having: first_name, last_name, age, email, and secret.
    // Is is computer-generated, so will be good data.
    //
    //$data = $_GET['people'];              // Testing w/ GET and a query string w/ Postman
    //$people = json_decode($data, true);   // true: return assoc array
    $people = json_decode(file_get_contents("php://input"), true);   // Sending raw JSON in a POST w/ Postman

    // Sort by age first so that the order in the emails list matches the people order
    // as inserted into the DB. Note that if there are exactly 2 people in the data then
    // the sort order is undefined. If data contains less than 2 people, then the sort
    // function is not called. 
    usort($people['data'], function ($a, $b) {
        if ((int)$a["age"] == (int)$b["age"]) {
            return 0;
        }
        return ((int)$a["age"] < (int)$b["age"]) ?  1: -1;   // Sort in descending order per requirement.
    } );

    // Create CSV of all the emails, and
    // add field `name` as the combination of first and last names.
    $emails = array();

    foreach ($people['data'] as &$person) {
        array_push($emails, $person["email"]);
        $person['name'] = $person["first_name"] . ' ' . $person["last_name"];
    }

    // Requirements mentioned that the email list is to be stored in the DB as JSON.
    $emails_json = array("emails" => implode(', ', $emails));

    // Add new record to the DB
    // Timestamp column not needed here -- is set up in the MySQL table to default to current_timestamp.
    DB::table('people')->insert(
        [
            'src_ip_addr' => $_SERVER['REMOTE_ADDR'],
            'emails' => json_encode($emails_json),
            'people' => json_encode($people)
        ]
    );
} );

Route::match(['get'], '/people', function () {
    // Read
    echo "\nGET\n";

    // Selecting only the first 8 records for this exercise... 
    // Ideally, records should be read in chunks for display in a virtual-scrolling table.
    $records = DB::table('people')->select('*')->orderBy('pk', 'asc')->limit(8)->get();

    return json_encode($records);
} );

Route::delete('/people/{pklist}', function() {
    // Delete

    // Client should contain affordances for deleting records ONLY AFTER the first two, to 
    // conform to the requirements. 
    // Client should do a GET afterwards to update its view, closed-loop. I.e., the DB is 
    // the sole source of truth as to what records it contains.
    // Laravel docs say that Eloquent takes care of creating a prepared statement.
    // If you have control of the client code, you can ensure good data. 

    // See https://webdevetc.com/programming-tricks/laravel/laravel-routes/cheatsheet for the 
    // Route::current()->parameters() idea. Attempting to use $pklist as a function arg, it 
    // had a spurious "1" at the end of the string (at least when sending a DELETE from Postman)
    // and web search yielded no info on that behavior.
    $pklist = Route::current()->parameters()["pklist"];

    $pks = explode(',', $pklist);  // Expecting a CSV string

    // Requirements say to disallow deleting pks 1 and 2
    $pks = array_filter($pks, function($e) {
        return (($e !== "1") and ($e !== "2"));   // Removes multiple occurances
    });

    $ret = DB::table('people')->whereIn('pk', $pks)->delete(); 
    return $ret;
} );

Route::match(['get'], '/', function () {
    return "Laravel 5";
} );
